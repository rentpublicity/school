<?php
$config = array('alumno' => array(
    								array(
                                	'field'=>'tipo_documento',
                                	'label'=>'Tipo de Documento',
                                	'rules'=>'required|trim|xss_clean'
                                ),
                                                                array(
                                	'field'=>'documento',
                                	'label'=>'Documento',
                                	'rules'=>'required|trim|callback_unico_documento'
                                ),
								array(
                                	'field'=>'primer_nombre',
                                	'label'=>'Primer Nombre',
                                	'rules'=>'required|trim|xss_clean'
                                ),
                                                                array(
                                	'field'=>'fecha_nacimiento',
                                	'label'=>'Fecha nacimiento',
                                	'rules'=>'required|trim|xss_clean|callback_edad_inscripcion'
                                ),
                                                                array(
                                	'field'=>'primer_apellido',
                                	'label'=>'Primer Apellido',
                                	'rules'=>'required|trim|xss_clean'
                                ),
								array(
                                	'field'=>'telefono',
                                	'label'=>'Teléfono',
                                	'rules'=>'required|trim|xss_clean'
                                ),
								array(
                                	'field'=>'sexo',
                                	'label'=>'Sexo',
                                	'rules'=>'required|trim|xss_clean'
                                ),
								array(
                                	'field'=>'fecha_ingreso',
                                	'label'=>'Ingreso',
                                	'rules'=>'required|trim|xss_clean'
                                ),
                                                                array(
                                	'field'=>'fecha_retiro',
                                	'label'=>'Retiro',
                                	'rules'=>'trim|xss_clean'
                                ),
								array(
                                	'field'=>'direccion',
                                	'label'=>'Dirección',
                                	'rules'=>'required|trim|xss_clean'
                                ),
                                                                array(
                                	'field'=>'email',
                                	'label'=>'Correo',
                                	'rules'=>'required|callback_unico_email|trim|xss_clean'
                                )
    
		),
                'parametros' => array(
                                                                array(
                                	'field'=>'valor',
                                	'label'=>'Parámetro',
                                	'rules'=>'required|trim|xss_clean'
                                )
                ),
                'materia' => array(
                                                                array(
                                	'field'=>'codigo',
                                	'label'=>'Código',
                                	'rules'=>'required|trim|xss_clean|callback_unico_codigo'
                                ),
                                                                array(
                                	'field'=>'nombre',
                                	'label'=>'Descripción',
                                	'rules'=>'required|trim|xss_clean|callback_unico_nombre'
                                ),
                ),
                'grado' => array(
                                                                array(
                                	'field'=>'nombre',
                                	'label'=>'Descripción',
                                	'rules'=>'required|trim|xss_clean|callback_unico_grado'

                                )
                )
    );