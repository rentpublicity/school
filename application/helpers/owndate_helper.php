<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('BASEPATH') OR exit('No direct script access allowed');
if(!function_exists('dateFormat'))
{
    function dateFormat($givenDate,$format='d/m/Y')
    {
        return date($format, strtotime($givenDate));
    }
}

