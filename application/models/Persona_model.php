<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class persona_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }

    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    
    function getById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('persona')->row();
    }
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }   
	
	function count($table){
		return $this->db->count_all($table);
	}

    public function search($pesquisa, $de, $ate){
        
        if($pesquisa != null){
            $this->db->like('documento',$pesquisa);
        }

        if($de != null){
            $this->db->where('cadastro >=' ,$de);
            $this->db->where('cadastro <=', $ate);
        }
        $this->db->limit(10);
        return $this->db->get('documentos')->result();
    }
    
    public function  getTipoDocumentos(){
        $this->db->select("*");
        $this->db->from("tipo_documento");
        $query = $this->db->get();
        $result =  $query->result();
        return $result;
        
    }
    public function getTipo_DOcumento($id){
        $this->db->select("*");
        $this->db->from("tipo_documento");
        $this->db->where(["id" => $id]);
        $query = $this->db->get();
        $result =  $query->row();
        return $result;
    }

}

/* End of file alumno_model.php */

