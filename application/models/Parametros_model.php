<?php
class parametros_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    
    function getAll(){
        $this->db->select("*,p.idparametro as id");
        $this->db->from("parametro p");
        $result =  $this->db->get()->result();
        return $result;
    }

    function getById($id){
        $this->db->select('*');
        $this->db->from('parametro');
        $this->db->where('idparametro',$id);
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }   
	
    function count($table){
            return $this->db->count_all($table);
    }

    
}