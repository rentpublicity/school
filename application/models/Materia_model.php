<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class materia_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }

    function getMaterias(){
        $this->db->select("m.*");
        $this->db->from('materia m');
        return  $this->db->get()->result();
        return $result;
    }
    
    function getById($codigo){
        $this->db->select("m.*,u.usuario as usuario_registro");
        $this->db->from('materia m');
        $this->db->join('usuario u','m.usuario_registro = u.id');
        ($codigo!=NULL ? $this->db->where(['m.codigo' => $codigo]) : null);
        $query = $this->db->get();
        $result =  $query->row();
        return $result;
    }
    
    function getBynombre($id,$nombre){
        if(($nombre)!= NULL){
            $campo = "id !=";
        }
        else
            $campo = "id =";
        $check = $this->db->get_where('materia', array($campo => $id, 'nombre' => $nombre), TRUE);
        return ($check->num_rows() > 0);
        
    }
    
    function getBycodigo($id,$codigo){
        if(($codigo)!= NULL){
            $campo = "id !=";
        }
        else
            $campo = "id =";
        $check = $this->db->get_where('materia', array($campo => $id,'codigo' => $codigo), TRUE);
        return ($check->num_rows() > 0);
        
    }
    
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }   

}

/* End of file alumno_model.php */

