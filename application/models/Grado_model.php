<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class grado_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getGrados($id = NULL) {
        $this->db->select("*");
        $this->db->from('grado g');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getById($id) {
        $this->db->select("g.*, u.usuario");
        $this->db->from('grado g');
        $this->db->join('usuario u','g.usuario_registro = u.id');
        ($id != NULL ? $this->db->where(['g.id' => $id]) : null);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function add($table, $data) {
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function edit($table, $data, $fieldID, $ID) {
        $this->db->where($fieldID, $ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
            return TRUE;
        }

        return FALSE;
    }

    function delete($table, $fieldID, $ID) {
        $this->db->where($fieldID, $ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function count($table) {
        return $this->db->count_all($table);
    }

}

/* End of file alumno_model.php */

