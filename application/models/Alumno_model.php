<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class alumno_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }

    function getAlumnos($id = NULL){
        $this->db->select("p.*,u.id usuario_id,u.usuario,TIMESTAMPDIFF(YEAR,p.fecha_nacimiento,now()) as edad,a.id as alumno_id, a.fecha_ingreso alumno_fecha_ingreso,a.fecha_registro alumno_fecha_regisitro,a.fecha_retiro alumno_fecha_retiro,t.nombre as tipo_documento,a.status");
        $this->db->from('alumno a');
        $this->db->join('persona p','a.persona_id = p.id and a.activo = 1');
        $this->db->join('usuario u','a.usuario_registro = u.id');
        $this->db->join('tipo_documento t','p.tipo_documento_id = t.id');
        ($id!=NULL ? $this->db->where(['a.id' => $id]) : null);
        $query = $this->db->get();
        $result =  $query->result();
        return $result;
    }
    function getStatus(){
        $result = $this->db->query('SHOW COLUMNS FROM alumno like \'%status%\'')->row();
        
        $status = str_replace(["enum(",")","'"],["","",""], $result->Type);
        $status = explode(",",$status);
        return $status;
        
        
    }
    function getById($id){
        $this->db->select("p.*,u.id usuario_id,u.usuario,TIMESTAMPDIFF(YEAR,p.fecha_nacimiento,now()) as edad,a.usuario_registro as alumno_usuario_registro,a.fecha_registro as alumno_fecha_registro,a.id as alumno_id, a.fecha_ingreso alumno_fecha_ingreso,a.fecha_retiro alumno_fecha_retiro,t.nombre as tipo_documento,a.representante_principal,a.representante_secundario,a.status");
        $this->db->from('persona p');
        $this->db->join('alumno a','a.persona_id = p.id and p.activo =1 and a.activo=1');
        $this->db->join('usuario u','a.usuario_registro = u.id');
        $this->db->join('tipo_documento t','p.tipo_documento_id = t.id');
        ($id!=NULL ? $this->db->where(['a.id' => $id]) : null);
        $query = $this->db->get();
        $result =  $query->row();
        return $result;
    }
    
    function getByDocumento($tipo,$documento){
        $this->db->select("p.*,p.id as id_p,a.id");
        $this->db->from("persona p");
        $this->db->join("alumno a","a.persona_id = p.id and a.activo = 1",'left');
        $this->db->where(['tipo_documento_id' => $tipo,'documento' => $documento,"a.id" => NULL]);
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }   
	
    function count($table){
            return $this->db->count_all($table);
    }

    public function search($pesquisa, $de, $ate){
        
        if($pesquisa != null){
            $this->db->like('documento',$pesquisa);
        }

        if($de != null){
            $this->db->where('cadastro >=' ,$de);
            $this->db->where('cadastro <=', $ate);
        }
        $this->db->limit(10);
        return $this->db->get('documentos')->result();
    }

}

/* End of file alumno_model.php */

