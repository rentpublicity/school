<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipo_documento_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }

    function getAll(){
        $this->db->select("tp.*,date_format(tp.fecha_registro,'%d/%m/%Y') fecha_registro");
        $this->db->from('tipo_documento tp');
        $this->db->where(['tp.activo' => 1]);
        $query = $this->db->get();
        $result =  $query->result();
        return $result;
    }
    
    function getById($id){
        $this->db->select("tp.*,date_format(tp.fecha_registro,'%d/%m/%Y') fecha_registro");
        $this->db->from('tipo_documento tp');
        $this->db->where(['tp.id' => $id,'tp.activo'=> 1]);
        $query = $this->db->get();
        $result =  $query->result();
        return $result;
    }
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }   
	
    function count($table){
        return $this->db->count_all($table);
    }

    

}

/* End of file tipo_documento_model.php */

