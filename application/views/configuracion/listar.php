<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gears"></i> Configuración
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body" style="text-align: center; padding: 20px">   
                        <a href="<?php echo base_url(); ?>index.php/materia">
                            <span>Materias</span>
                        </a>
                        <a href="<?php echo base_url(); ?>index.php/seccion">
                            <span>Secciones</span>
                        </a>
                        <a href="<?php echo base_url(); ?>index.php/grado">
                            <span>Grados</span>
                        </a>
                        <a href="<?php echo base_url(); ?>index.php/cargo">
                            <span>Cargos</span>
                        </a>
                        <a href="<?php echo base_url(); ?>index.php/tipo_evento">
                            <span>Tipos de evento</span>
                        </a>
                        <a href="<?php echo base_url(); ?>index.php/tipo_documento">
                            <span>Tipos de documento</span>
                        </a>
                        <a href="<?php echo base_url(); ?>index.php/usuario">
                            <span>Usuarios</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>