<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gear"></i> Tipos de documentos
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/tipo_documento/crear">
                                <button type="button" class="btn btn-success">
                                    Nuevo
                                </button>
                            </a>                            
                        </div>
                        <div style="float: right; text-align: center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <table id="tabla" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Descripción</th>
                        </tr>
                    </thead>
                    <tbody class="filas_tabla">
                        <?php
                        foreach($tipo_documentos as $tipo){ ?>
                            <tr class="seleccionar" data-number_id ="<?php echo $tipo->id; ?>">
                                <td >
                                    <?php echo $tipo->nombre; ?>
                                </td>
                            </tr>
                        <?php }
                        ?>
                    </tbody>            
                </table>
            </div>        
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>application/views/tipo_documento/js/listar.js"></script>