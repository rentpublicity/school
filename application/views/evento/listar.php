<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gift"></i> Eventos
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/evento/crear">
                                <button type="button" class="btn btn-success">
                                    Nuevo
                                </button>
                            </a>                            
                        </div>
                        <div style="float: right; text-align: center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <table id="tabla" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Tipo de evento</th>
                            <th>Descripción</th>
                            <th>Fecha</th>
                            <th>Responsable</th>
                        </tr>
                    </thead>
                    <tbody class="filas_tabla">
                        <tr class="seleccionar">
                            <td>
                                tipo de evento
                            </td>
                            <td>
                                descripcion
                            </td>
                            <td>
                                fecha
                            </td>
                            <td>
                                responsable
                            </td>
                        </tr>
                    </tbody>            
                </table>
            </div>        
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>application/views/evento/js/listar.js"></script>