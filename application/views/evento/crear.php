<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gift"></i> Eventos | Nuevo
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/evento">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <form action="<?php echo current_url(); ?>" id="form" method="post">
                        <div class="box box-info">
                            <div class="box-body"> 
                                <?php if (!empty($custom_error)) { ?>
                                    <div class="alert alert-danger"> <?php echo $custom_error; ?> </div>
                                <?php }
                                ?>
                                <div class="col-md-4">
                                    <label>Tipo de evento:</label> 
                                    <select id="tipo_evento" name="tipo_evento" class="form-control error">
                                        <option value="">Seleccione</option>
                                    </select>
                                    <br>                                    
                                    <label>Responsable:</label> 
                                    <select id="responsable" name="responsable" class="form-control error">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Fecha:</label> 
                                    <input id="fecha_evento" type="date" name="fecha_evento" value="" class="form-control">
                                    <br>
                                    <label>Descripción:</label> 
                                    <input id="descripcion" type="text" name="descripcion" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Observaciones:</label> 
                                    <input id="observacion" type="text" name="observacion" value="" class="form-control" style="height: 113px">
                                </div>
                            </div>
                            <div style="text-align: center; margin: 20px">
                                <a>
                                    <button type="submit" class="btn btn-success">
                                        Aceptar
                                    </button>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/evento">
                                    <button type="button" class="btn btn-default">
                                        Cancelar
                                    </button>
                                </a>
                            </div>
                            <br>
                        </div>  
                    </form> 
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url() ?>assets/js/validate.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').validate({
            rules: {
                tipo_evento: {required: true},
                descripcion: {required: true},
                responsable: {required: true},
                fecha_evento: {required: true}
            },
            messages: {
                tipo_evento: {required: 'Campo requerido.'},
                descripcion: {required: 'Campo requerido.'},
                responsable: {required: 'Campo requerido.'},
                fecha_eventos: {required: 'Campo requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>