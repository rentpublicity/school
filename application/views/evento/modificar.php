<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gift"></i> Eventos | Modificar
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/evento/consultar">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-body">    
                            <div class="col-md-4">
                                    <label>Tipo de evento:</label> 
                                    <select id="tipo_evento" name="tipo_evento" class="form-control error">
                                        <option value="">Seleccione</option>
                                    </select>
                                    <br>                                    
                                    <label>Responsable:</label> 
                                    <select id="responsable" name="responsable" class="form-control error">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Fecha:</label> 
                                    <input id="fecha_evento" type="date" name="fecha_evento" value="" class="form-control">
                                    <br>
                                    <label>Descripción:</label> 
                                    <input id="descripcion" type="text" name="descripcion" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Observaciones:</label> 
                                    <input id="observacion" type="text" name="observacion" value="" class="form-control" style="height: 113px">
                                </div>
                        </div>
                        <div style="text-align: center; margin: 20px">
                            <a href="<?php echo base_url(); ?>index.php/evento/consultar">
                                <button type="button" class="btn btn-success">
                                    Aceptar
                                </button>
                            </a>
                            <a href="<?php echo base_url(); ?>index.php/evento/consultar">
                                <button type="button" class="btn btn-default">
                                    Cancelar
                                </button>
                            </a>
                        </div>
                        <br>
                    </div>                    
                </div>
            </div>
        </div>
    </section>
</div>