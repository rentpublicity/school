<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-calendar-check-o"></i> Asistencia | Nuevo
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/asistencia">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <form action="<?php echo current_url(); ?>" id="form" method="post">
                        <div class="box box-info">
                            <div class="box-body"> 
                                <?php if (!empty($custom_error)) { ?>
                                    <div class="alert alert-danger"> <?php echo $custom_error; ?> </div>
                                <?php
                                } ?>
                                <div class="col-md-4">
                                    <label>Tipo de documento:</label> 
                                    <select id="tipo_documento" name="tipo_documento" class="form-control error">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Documento:</label> 
                                    <input id="documento" type="text" name="documento" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Fecha de asistencia:</label> 
                                    <input id="fecha_asistencia" type="date" name="fecha_asistencia" value="" class="form-control">
                                </div>
                            </div>
                            <div style="text-align: center; margin: 20px">
                                <a>
                                    <button type="submit" class="btn btn-success">
                                        Aceptar
                                    </button>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/asistencia">
                                    <button type="button" class="btn btn-default">
                                        Cancelar
                                    </button>
                                </a>
                            </div>
                            <br>
                        </div>  
                    </form> 
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>assets/js/validate.js"></script>
<script type="text/javascript">
      $(document).ready(function(){
           $('#formAsistencia').validate({
            rules :{
                  tipo_documento:{ required:true },
                  documento:{ required:true },
                  fecha_asistencia:{ required:true }                  
            },
            messages:{
                tipo_documento:{ required:'Campo requerido.' },
                documento:{ required:'Campo requerido.' },
                fecha_asistencia:{ required:'Campo requerido.' }
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });
      });
</script>