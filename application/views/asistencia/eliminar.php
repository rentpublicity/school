<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-calendar-check-o"></i> Asistencia | Eliminar
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/asistencia/consultar">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div style="text-align: center" class="box-body">    
                            <i style="font-size: 100px; color: #FF3C3C" class="fa fa-trash"></i>
                            <br>
                            <h2>¿Desea eliminar los datos de la asistencia?</h2>
                        </div>
                        <div style="text-align: center; margin: 20px">
                            <a href="<?php echo base_url(); ?>index.php/asistencia/consultar">
                                <button type="button" class="btn btn-success">
                                    Aceptar
                                </button>
                            </a>
                            <a href="<?php echo base_url(); ?>index.php/asistencia/consultar">
                                <button type="button" class="btn btn-default">
                                    Cancelar
                                </button>
                            </a>
                        </div>
                        <br>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>