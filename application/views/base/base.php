<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>School</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/icono.ico">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/morris.js/morris.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>dist/sweetalert.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts-google.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/datatables.net-bs/css/buttons.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sitio.css">
        <script type="text/javascript"  src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <header class="main-header">
            <a href="<?php echo base_url(); ?>index.php/dashboard" class="logo">
                <span class="logo-mini">SC</span>
                <span class="logo-lg">School</span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo base_url(); ?>assets/img/user.jpg" class="user-image" alt="">
                                <span class="hidden-xs">
                                    <?php echo $this->session->userdata('user')?>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <img src="<?php echo base_url(); ?>assets/img/user.jpg" class="img-circle" alt="">

                                    <p>
                                        <?php echo $this->session->userdata('nombre_usuario') . " " . $this->session->userdata('apellido_usuario')?>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="<?php echo base_url(); ?>index.php/dashboard/salir" class="btn btn-default btn-flat">Cerrar sesión</a>
                                    </div>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">    
            <section class="sidebar">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo base_url(); ?>assets/img/user.jpg" class="img-circle" alt="">
                    </div>
                    <div class="pull-left info">
                        <p>
                            Nombre Usuario
                        </p>
                    </div>
                </div>
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">Menú</li>
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/dashboard">
                            <i class="fa fa-home"></i> <span>Inicio</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/alumno">
                            <i class="fa fa-graduation-cap"></i> <span>Alumnos</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/empleado">
                            <i class="fa fa-book"></i> <span>Empleados</span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/inscripcion">
                            <i class="fa fa-pencil-square-o"></i> <span>Inscripciones</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/curso">
                            <i class="fa fa-calendar"></i> <span>Cursos</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/asistencia">
                            <i class="fa fa-calendar-check-o"></i> <span>Asistencia</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/evento">
                            <i class="fa fa-gift"></i> <span>Eventos</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-gears"></i> <span>Configuración</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/materia">
                                    <span>Materias</span>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/seccion">
                                    <span>Secciones</span>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/grado">
                                    <span>Grados</span>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/cargo">
                                    <span>Cargos</span>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/tipo_evento">
                                    <span>Tipos de evento</span>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/tipo_documento">
                                    <span>Tipos de documento</span>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/usuario">
                                    <span>Usuarios</span>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/parametros">
                                    <span>Parámetros</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </section>
        </aside>
        <!-- Vista -->
        <?php if (isset($view)) { ?>
            <?php
            $this->load->view($view);
        }
        ?>

        <script src="<?php echo base_url(); ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <script src="<?php echo base_url(); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/raphael/raphael.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/morris.js/morris.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/moment/min/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/fastclick/lib/fastclick.js"></script>
        <script src="<?php echo base_url(); ?>dist/js/adminlte.min.js"></script>
        <script src="<?php echo base_url(); ?>dist/js/pages/dashboard.js"></script>
        <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/chart.js/Chart.js"></script>
        <script src="<?php echo base_url(); ?>dist/sweetalert-dev.js"></script>
        <script src="<?php echo base_url(); ?>dist/sweetalert.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/datatables.net-bs/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/datatables.net-bs/js/buttons.flash.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/datatables.net-bs/js/jszip.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/datatables.net-bs/js/pdfmake.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/datatables.net-bs/js/vfs_fonts.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/datatables.net-bs/js/buttons.html5.min.js"></script>
        <script src="<?php echo base_url(); ?>bower_components/datatables.net-bs/js/buttons.print.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
    </body>
</html>
