<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>School</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="assets/img/icono.ico">        
        <link rel='stylesheet' href='css/fonts.css'>
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/supersized.css">
        <link rel="stylesheet" href="css/style.css">            
        <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">  
        
    </head>
    <body>
        <?php echo base_url();?>
        <div class="page-container">
            <!--<img src="img/logo_grupo.jpeg" style="width: 200px;" alt="Universal Service, S.A.S">-->
            <h1>School</h1>
            <input type="text" id="usuario" name="usuario" class="username" placeholder="Usuario"><br>
            <input type="password" id="password" name="password" class="password" placeholder="Contraseña"><br>
            <button id="login">Iniciar Sesión</button>
            <div class="error"><span>+</span></div>
            <br><br>
            <br><br>
        </div>  
        <!-- Javascript -->
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/supersized.3.2.7.min.js"></script>
        <script src="js/supersized-init.js"></script>
        <script src="dist/sweetalert.min.js"></script>   
    </body>

</html>

