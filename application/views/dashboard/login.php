<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>School</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/icono.ico">
        <!-- CSS -->
        <link rel='stylesheet' href="<?php echo base_url(); ?>assets/css/fonts.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/supersized.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">   
        <!-- Alert -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/sweetalert.css">
        <script src="<?php echo base_url(); ?>dist/sweetalert.min.js"></script>  
    </head>
    <body>
        <div class="page-container">
            <br><br>
            <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width: 200px;" alt="Universal Service, S.A.S">
            <br>
            <br>
            <h1 style="color: #1a2226">School</h1>
            <form  class="form-vertical" id="formLogin" method="post" action="">
                <input type="text" id="usuario" name="usuario" class="username" placeholder="Usuario"><br>
                <input type="password" id="contraseña" name="password" class="password" placeholder="Contraseña"><br>
                <button id="login">Iniciar Sesión</button>
                <div class="error"><span>+</span></div>
            </form>
            <br><br>
            <br><br>
        </div>  
        <!-- Javascript -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.8.2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/supersized.3.2.7.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/supersized-init.js"></script>
        <script src="<?php echo base_url()?>assets/js/validate.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#usuario').focus();
                $("#formLogin").validate({
                     rules :{
                          usuario: { required: true},
                          password: { required: true}
                    },
                    messages:{
                          usuario: { required: 'Campo Requerido.'},
                          password: {required: 'Campo Requerido.'}
                    },
                   submitHandler: function( form ){       
                        var datos = $( form ).serialize();
                        $.ajax({
                          type: "POST",
                          url: "<?php echo base_url();?>index.php/dashboard/verificarLogin?ajax=true",
                          data: datos,
                          dataType: 'json',
                          success: function(data)
                          {
                            if(data.result == true){
                                window.location.href = "<?php echo base_url();?>index.php/dashboard";
                            }
                            else{
                                $('#call-modal').trigger('click');
                            }
                          }
                          });

                          return false;
                    },

                    errorClass: "help-inline",
                    errorElement: "span",
                    highlight:function(element, errorClass, validClass) {
                        $(element).parents('.control-group').addClass('error');
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).parents('.control-group').removeClass('error');
                        $(element).parents('.control-group').addClass('success');
                    }
                });

            });
        </script>
        <a href="#notification" id="call-modal" role="button" class="btn" data-toggle="modal" style="display: none ">notification</a>

        
    </body>

</html>

