<div class="content-wrapper" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1>
            <i class="fa fa-home"></i> 
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-4 col-xs-12">
                <a href="#">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3 id="">0</h3>
                            <p>Alumnos</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-graduation-cap"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-12">
                <a href="#">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3 id="">0</h3>
                            <p>Empleados</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-book"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-12">
                <a href="#">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3 id="">0</h3>
                            <p>Representantes</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users"></i>
                        </div>
                    </div>
                </a>
            </div>
        </div>  
    </section>
</div>
