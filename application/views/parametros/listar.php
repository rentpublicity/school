<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gear"></i> Parámetros
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <table id="tabla" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Parametro</th>
                            <th>Tipo</th>
                            <th>Valor</th>
                            <th>Descripción</th>
                        </tr>
                    </thead>
                    <tbody class="filas_tabla">
                        <?php foreach($parametros as $parametro){ ?>
                            <tr data-value =  "<?php echo $parametro->id ?>" class="seleccionar">
                                <td>
                                    <?php echo $parametro->nombre ?>
                                </td>
                                <td>
                                    <?php echo $parametro->tipo_dato ?>
                                </td>
                                <td>
                                    <?php echo $parametro->valor ?>
                                </td>
                                <td>
                                    <?php echo $parametro->descripcion ?>
                                </td>
                            </tr>
                        <?php 
                            }
                        ?>
                    </tbody>            
                </table>
            </div>        
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>application/views/parametros/js/listar.js"></script>
<script>
    $(".seleccionar").click(function(){
        var number = $(this).attr('data-value');
        var base_url = "<?php echo base_url()?>";
        window.location.href = base_url+"index.php/parametros/modificar/"+number;
    });
</script>