<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gear"></i> Parámetro | Modificar
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/parametros/">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="box box-info">
                        <form action="<?php echo current_url(); ?>" id="form" method="post">
                            <div class="box-body">
                                <?php
                                if($this->session->flashdata('success') != null){?>
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i>¡Éxito!</h4>
                                        <?php echo $this->session->flashdata('success');?>
                                    </div>
                                <?php }?>

                                <?php if ($this->session->flashdata('error') != null || !empty(@$custom_error)){?>
                                      <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i> ¡Error!</h4>
                                        <?php echo $this->session->flashdata('error') . $custom_error;?>
                                    </div>
                                <?php }?>
                                
                                <div class="col-md-4 col-md-offset-4">
                                    <label>Parametro:</label> <a><?php echo $parametro->nombre?></a>
                                    <input id="valor" type="number" name="valor"  class="form-control" value="<?php echo $parametro->valor?>">
                                    <br>
                                </div>
                            </div>
                            <div style="text-align: center; margin: 20px">
                                <a>
                                    <button type="submit" class="btn btn-success">
                                        Aceptar
                                    </button>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/parametros">
                                    <button type="button" class="btn btn-default">
                                        Cancelar
                                    </button>
                                </a>
                            </div>
                        </form>
                        <br>
                    </div>                    
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>assets/js/validate.js"></script>
