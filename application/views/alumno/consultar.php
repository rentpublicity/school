<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-graduation-cap"></i> Alumnos | Consultar
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/alumno">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                        <div style="float: right; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/alumno/modificar/<?php echo $id?>">
                                <button type="button" class="btn btn-warning">
                                    Modificar
                                </button>
                            </a>
                            <a href="<?php echo base_url(); ?>index.php/alumno/eliminar/<?php echo $id?>">
                                <button type="button" class="btn btn-danger">
                                    Eliminar
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-9">
                    <div class="box box-info">
                        <h3 style="margin-left: 20px">Alberto Atencio</h3>
                            <div class="box-body">    
                                <div class="col-md-6">                                    
                                    <label>Tipo de documento:</label> <?php echo $alumno->tipo_documento?> <br>
                                    <label>Documento:</label> <?php echo $alumno->documento?> <br>
                                    <label>Nombres:</label> <?php echo $alumno->primer_nombre . " ". $alumno->segundo_nombre?> <br>
                                    <label>Apellidos:</label> <?php echo $alumno->primer_apellido ." ".$alumno->segundo_apellido?> <br>
                                    <label>Edad:</label> <?php echo $alumno->edad . " año(s) ". "(".dateFormat('d/m/Y',$alumno->fecha_nacimiento) . ")"?> <br>
                                    <label>Satus:</label> <?php echo $alumno->status?>
                                </div>
                                <div class="col-md-6">                                    
                                    <label>Sexo:</label> <?php echo $alumno->sexo == 'f' ? "Femenino" : "Masculino"; ?><br>
                                    <label>Correo:</label> <?php echo $alumno->email?><br>
                                    <label>Teléfono:</label> <?php echo $alumno->telefono?> <br>
                                    <label>Dirección:</label> <?php echo $alumno->direccion?><br>     
                                    <label>Ingreso:</label> <?php echo dateFormat('d/m/Y',$alumno->alumno_fecha_ingreso)?> <br>
                                    <label>Retiro:</label> <?php echo $alumno->alumno_fecha_retiro!= NULL ? dateFormat('d/m/Y',$alumno->alumno_fecha_retiro) : ""?> <br>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box box-info">
                        <h3 style="margin-left: 5px">Datos de registro</h3>
                            <div class="box-body">    
                                <label>Usuario:</label> <?php echo $alumno->usuario?>  <br>
                                <label>Registro:</label> <?php echo $alumno->alumno_fecha_registro!= NULL ? dateFormat('d/m/Y',$alumno->alumno_fecha_registro) : ""?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">                
                <div class="col-md-9">                    
                    <div class="box box-info">
                        <h3 style="margin-left: 20px">Inscripción</h2>
                            <div class="box-body">                                
                                <div class="col-md-6">
                                    <label>Fecha:</label> 01/01/2018 <br>
                                </div>
                                <div class="col-md-6">
                                    <label>Grado:</label> 6 <br>
                                    <label>Sección:</label> A <br>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box box-info">
                        <h3 style="margin-left: 5px">Datos de registro</h3>
                            <div class="box-body">    
                                <label>Usuario:</label> User1 <br>
                                <label>Registro:</label> 01/01/2018
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">                
                <div class="col-md-12">                    
                    <div class="box box-info">
                        <h3 style="margin-left: 20px">Cursos</h2>
                            <div class="box-body">                                
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Grado</th>
                                            <th>Sección</th>
                                            <th>Materia</th>
                                            <th>Maestro</th>                                            
                                            <th>Calificación</th>                                            
                                            <th>Inicio</th>
                                            <th>Fin</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody class="filas_tabla">
                                        <tr class="seleccionarCurso">
                                            <td>
                                                6
                                            </td>
                                            <td>
                                                A
                                            </td>
                                            <td>
                                                Matemáticas
                                            </td>
                                            <td>
                                                Pedro Pérez
                                            </td>                                            
                                            <td>
                                                19
                                            </td>                                            
                                            <td>
                                                01/01/2018
                                            </td>
                                            <td>
                                                31/12/2018
                                            </td>
                                        </tr>
                                    </tbody>            
                                </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>