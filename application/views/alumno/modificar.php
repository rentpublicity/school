<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-graduation-cap"></i> Alumnos | Modificar
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/alumno/consultar/<?php echo $id ?>">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <form action="<?php echo current_url(); ?>" id="form" method="post">
                            <div class="box box-info">
                                <div class="box-body">
                                <?php
                                if($this->session->flashdata('success') != null){?>
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i>¡Éxito!</h4>
                                        <?php echo $this->session->flashdata('success');?>
                                    </div>
                                <?php }?>

                                <?php if ($this->session->flashdata('error') != null || !empty(@$custom_error)){?>
                                      <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i> ¡Error!</h4>
                                        <?php echo $this->session->flashdata('error') . $custom_error;?>
                                    </div>
                                <?php }?>
                                            <div class="col-md-4">
                                                    <label>Tipo de documento:</label> 
                                                    <select class="form-control" name="tipo_documento">
                                                            <option value="">Seleccione</option>
                                                            <?php foreach ($tipo_documentos as $tipo) { ?>
                                                                    <option value="<?php echo $tipo->id ?>" <?php echo $alumno->tipo_documento_id == $tipo->id ? "selected" : "" ?>><?php echo $tipo->nombre ?></option>
                                                                    <?php
                                                            }
                                                            ?>
                                                    </select>
                                                    <br>
                                                    <label>Documento:</label> 
                                                    <input type="hidden" name="id_persona" value="<?php echo $alumno->id ?>" class="form-control">
                                                    <input type="text" name="documento" value="<?php echo $alumno->documento ?>" class="form-control">
                                                    <br>
                                                    <label>Primer nombre:</label> 
                                                    <input type="text" name="primer_nombre" value="<?php echo $alumno->primer_nombre ?>" class="form-control">
                                                    <br>
                                                    <label>Segundo nombre:</label> 
                                                    <input type="text" name="segundo_nombre" value="<?php echo $alumno->segundo_nombre ?>"class="form-control">
                                                    <br>
                                                    <label>Primer apellido:</label> 
                                                    <input type="text" name="primer_apellido" value="<?php echo $alumno->primer_apellido ?>" class="form-control">
                                                    <br>
                                                    <label>Segundo apellido:</label> 
                                                    <input type="text" name="segundo_apellido" value="<?php echo $alumno->segundo_apellido ?>"class="form-control">
                                            </div>
                                            <div class="col-md-4">
                                                <label>Sexo:</label>
                                                <select id="sexo" name="sexo" class="form-control">
                                                    <option value="">Seleccione</option>
                                                    <option value="m" <?php echo ($alumno->sexo == 'm' ? "selected" : "")?>>Masculino</option>
                                                    <option value="f" <?php echo ($alumno->sexo == 'f' ? "selected" : "")?>>Femenino</option>
                                                </select>
                                                <br>
                                                <label>Fecha de nacimiento:</label> 
                                                <input type="date" name="fecha_nacimiento" value="<?php echo $alumno->fecha_nacimiento ?>" class="form-control">
                                                <br>
                                                <label>Correo:</label> 
                                                <input type="mail" name="email" value="<?php echo $alumno->email ?>" class="form-control">
                                                <br>
                                                <label>Teléfono:</label> 
                                                <input type="text" name="telefono" value="<?php echo $alumno->telefono ?>" class="form-control">
                                                <br>
                                                <label>Dirección:</label> 
                                                <textarea id="direccion"  name="direccion" style="height: 113px" name="direccion" class="form-control"><?php echo $alumno->direccion ?></textarea>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Ingreso:</label> 
                                                <input type="date" name="fecha_ingreso" value="<?php echo $alumno->alumno_fecha_ingreso ?>" class="form-control">
                                                <br>
                                                <label>Status:</label>
                                                <select id="status" name="status" class="form-control">
                                                    <option value="">Seleccione</option>
                                                    <?php foreach($status_alumnos as $status) { ?>
                                                        <option value="<?php echo $status?>" <?php echo ($alumno->status == $status ? "selected" : "")?>><?php echo $status ?></option>
                                                    <?php } ?>
                                                </select>
                                                <br>
                                                <label>Retiro:</label> 
                                                <input type="date" id="fecha_retiro" name="fecha_retiro" value="<?php echo $alumno->alumno_fecha_retiro ?>" class="form-control">
                                                <br>
                                                <label>Representante principal:</label> 
                                                <input id="representante_p" type="text" name="representante_p" value="<?php echo $alumno->representante_principal?>" class="erase_control form-control">
                                                <br>
                                                <label>Rrepresentante secundario:</label> 
                                                <input id="representante_s" type="text" name="representante_s" value="<?php echo $alumno->representante_secundario?>" class="erase_control form-control">
                                            </div>
                                    </div>
                                    <div style="text-align: center; margin: 20px">
                                            <a>
                                                    <button type="submit" class="btn btn-success">
                                                            Aceptar
                                                    </button>
                                            </a>
                                            <a href="<?php echo base_url(); ?>index.php/alumno/consultar/<?php echo $id ?>">
                                                    <button type="button" class="btn btn-default">
                                                            Cancelar
                                                    </button>
                                            </a>
                                    </div>
                                    <br>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>assets/js/validate.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#status').change(function(){
            if(!($('#status').val() == 'Expulsado' || $('#status').val() == 'Egresado')){
                $('#fecha_retiro').val("");
            }
        })
        $('#form').validate({
            rules :{
                  tipo_documento:{ required:true },
                  documento:{ required:true },
                  primer_nombre:{ required:true },
                  email : {email:true},
                  primer_apellido:{ required:true },
                  fecha_nacimiento:{ required:true },
                  telefono:{ required:true },
                  fecha_ingreso:{ required:true },
                  fecha_retiro:{required:{depends: function(){ return ($('#status').val() == 'Expulsado' || $('#status').val() == 'Egresado')}}},
                  sexo:{ required:true },
                  direccion:{ required:true },
                  representante_p :{ required:true }

            },
            messages:{
                tipo_documento:{ required:'Campo requerido.' },
                documento:{ required:'Campo requerido.' },
                email:{ email:"Ingrese un email valido"},
                primer_nombre:{ required:'Campo requerido.' },
                primer_apellido:{ required:'Campo requerido.' },
                fecha_nacimiento:{ required:'Campo requerido.' },
                telefono:{ required:'Campo requerido.' },
                fecha_ingreso:{ required:'Campo requerido.' },
                fecha_retiro:{required:'Campo requerido.' },
                sexo:{ required:'Campo requerido.' },
                direccion:{ required:'Campo requerido.' },
                representante_p:{ required:'Campo requerido.' }
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>