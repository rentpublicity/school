<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">    
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-graduation-cap"></i> Alumnos | Nuevo
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/alumno">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <form action="<?php echo current_url(); ?>" id="form" method="post">
                        <div class="box box-info">
                            <div class="box-body">
                                <?php
                                if($this->session->flashdata('success') != null){?>
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i>¡Éxito!</h4>
                                        <?php echo $this->session->flashdata('success');?>
                                    </div>
                                <?php }?>

                                <?php if ($this->session->flashdata('error') != null || !empty(@$custom_error)){?>
                                      <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i> ¡Error!</h4>
                                        <?php echo $this->session->flashdata('error') . $custom_error;?>
                                    </div>
                                <?php }?>
                                <div class="col-md-4">
                                    <label>Tipo de documento:</label> 
                                    <select id="tipo_documento" name="tipo_documento" class="form-control">
                                        <option value="">Seleccione</option>
                                        <?php
                                        foreach($tipo_documentos as $tipo){ ?>
                                        <option value="<?php echo $tipo->id ?>" <?php echo set_value('tipo_documento') ==  $tipo->id ? "selected" : "" ?>><?php echo $tipo->nombre?></option>
                                        <?php 
                                        }
                                        ?>
                                    </select>
                                    <br>
                                    <label>Documento:</label> 
                                    <input type="hidden" id ="id_persona" name="id_persona" value ="<?php echo set_value('id_persona')?>">
                                    <input id="documento" type="text" name="documento" class="form-control" value="<?php echo set_value('documento')?>">
                                    <br>                                    
                                    <label>Primer nombre:</label> 
                                    <input id="primer_nombre" type="text" name="primer_nombre" value="<?php echo set_value('primer_nombre')?>" class="erase_control form-control">
                                    <br>
                                    <label>Segundo nombre:</label> 
                                    <input id="segundo_nombre" type="text" name="segundo_nombre" value="<?php echo set_value('segundo_nombre')?>" class="erase_control form-control">
                                    <br>
                                    <label>Primer apellido:</label> 
                                    <input id="primer_apellido" type="text" name="primer_apellido" value="<?php echo set_value('primer_apellido')?>" class="erase_control form-control">
                                    <br>
                                    <label>Segundo apellido:</label> 
                                    <input id="segundo_apellido" type="text" name="segundo_apellido" value="<?php echo set_value('segundo_apellido')?>" class="erase_control form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Sexo:</label>
                                    <select id="sexo" name="sexo" class="erase_control form-control">
                                        <option value="">Seleccione</option>
                                        <option value="m" <?php echo set_value('sexo') == "m" ? "selected" : "" ?>>Masculino</option>
                                        <option value="f" <?php echo set_value('sexo') == "f" ? "selected" : ""?>>Femenino</option>
                                    </select>
                                    <br>
                                    <label>Fecha de nacimiento:</label> 
                                    <input id="fecha_nacimiento" type="date" name="fecha_nacimiento" value="<?php echo set_value('fecha_nacimiento')?>" class="erase_control form-control">
                                    <br>
                                    <label>Correo:</label> 
                                    <input id="email" type="email" name="email" value="<?php echo set_value('email')?>" class="erase_control form-control">
                                    <br>
                                    <label>Teléfono:</label> 
                                    <input id="telefono" type="text" name="telefono" value="<?php echo set_value('telefono')?>" class="erase_control form-control">
                                    <br>
                                    <label>Dirección:</label> 
                                    <textarea id="direccion"  style="height: 113px" name="direccion" class="erase_control form-control"><?php echo set_value('direccion')?></textarea>
                                </div>
                                <div class="col-md-4">
                                    <label>Ingreso:</label> 
                                    <input id="fecha_ingreso" type="date" name="fecha_ingreso" value="<?php echo empty(set_value('fecha_ingreso')) ? date('Y-m-d') : set_value('fecha_ingreso')?>" class="form-control">
                                    <br>
                                    <label>Representante principal:</label> 
                                    <input id="representante_p" type="text" name="representante_p" value="<?php echo set_value('representante_p')?>" class="erase_control form-control">
                                    <br>
                                    <label>Rrepresentante secundario:</label> 
                                    <input id="representante_s" type="text" name="representante_s" value="<?php echo set_value('representante_s')?>" class="erase_control form-control">
                                </div>
                            </div>
                            <div style="text-align: center; margin: 20px">
                                <a>
                                    <button type="submit" class="btn btn-success">
                                        Aceptar
                                    </button>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/alumno">
                                    <button type="button" class="btn btn-default">
                                        Cancelar
                                    </button>
                                </a>
                            </div>
                            <br>
                        </div>  
                    </form> 
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>assets/js/validate.js"></script>
<script type="text/javascript">
    function datosPersona(td,d){
        $.ajax({
            url: "<?php echo base_url(); ?>index.php/alumno/buscar_documento",
            type:'POST',
            dataType: "json",
            data: {
                td : td,
                d : d
            },
            success: function(data) {
                completar_datos(data);
            }
        });
    }
    function completar_datos(datos){
        $('#id_persona').val("");
        
        if(datos != null){
            $('.erase_control').each(function(){
                $(this).val("");
            })
            $('#id_persona').val(datos.id_p);
            $('#primer_nombre').val(datos.primer_nombre)
            $('#segundo_nombre').val(datos.segundo_nombre)
            $('#primer_apellido').val(datos.primer_apellido)
            $('#segundo_apellido').val(datos.segundo_apellido)
            $('#telefono').val(datos.telefono)
            $('#email').val(datos.email)
            $('#fecha_nacimiento').val(datos.fecha_nacimiento)
            $('#sexo').val(datos.sexo)
            $('#direccion').val(datos.direccion)
        }
        
    }
    $(document).ready(function(){
        $('#tipo_documento').change(function(){
            if($('#tipo_documento').val() != "" && $('#documento').val() != ""){
                var datos = datosPersona($('#tipo_documento').val(),$('#documento').val())
                
            }

        });

        $('#documento').blur(function(){
            if($('#tipo_documento').val() != "" && $('#documento').val()!=""){
                var datos = datosPersona($('#tipo_documento').val(),$('#documento').val())
                completar_datos(datos)
            }

        });

        $('#form').validate({
            rules :{
                  tipo_documento:{ required:true },
                  documento:{ required:true },
                  primer_nombre:{ required:true },
                  email : {required:true,email:true},
                  primer_apellido:{ required:true },
                  fecha_nacimiento:{ required:true },
                  telefono:{ required:true },
                  fecha_ingreso:{ required:true },
                  sexo:{ required:true },
                  direccion:{ required:true },
                  representante_p :{ required:true }

            },
            messages:{
                tipo_documento:{ required:'Campo requerido.' },
                documento:{ required:'Campo requerido.' },
                email:{ required:'Campo requerido.',email:"Ingrese un email valido"},
                primer_nombre:{ required:'Campo requerido.' },
                primer_apellido:{ required:'Campo requerido.' },
                fecha_nacimiento:{ required:'Campo requerido.' },
                telefono:{ required:'Campo requerido.' },
                fecha_ingreso:{ required:'Campo requerido.' },
                sexo:{ required:'Campo requerido.' },
                direccion:{ required:'Campo requerido.' },
                representante_p:{ required:'Campo requerido.' }
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>