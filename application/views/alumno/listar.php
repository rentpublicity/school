<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-graduation-cap"></i> Alumnos
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/alumno/crear">
                                <button type="button" class="btn btn-success">
                                    Nuevo
                                </button>
                            </a>                            
                        </div>
                        <div style="float: right; text-align: center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <table id="tabla" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Identificación</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Ingreso</th>                                            
                            <th>Retiro</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody class="filas_tabla">
                        <?php foreach ($alumnos as $alumno) { ?>

                            <tr class="seleccionar" data-number_id ="<?php echo $alumno->alumno_id; ?>">

                                <td>
                                    <a style="font-weight: bold; color: #000"><?php echo $alumno->tipo_documento . ": "?></a>  <?php echo $alumno->documento; ?>
                                </td>
                                <td>
                                    <?php echo $alumno->primer_nombre . ' ' . $alumno->segundo_nombre; ?>
                                </td>
                                <td>
                                    <?php echo $alumno->primer_apellido . ' ' . $alumno->segundo_apellido; ?>
                                </td>
                                <td>
                                    <?php echo dateFormat('d/m/Y',$alumno->alumno_fecha_ingreso); ?>
                                </td>
                                <td>
                                    <?php echo  $alumno->alumno_fecha_retiro!= NULL ? dateFormat('d/m/Y',$alumno->alumno_fecha_retiro) : ""?>
                                </td>
                                <td>
                                    <?php echo $alumno->status; ?>
                                </td>
                            </tr>
                        <?php }
                        ?>
                    </tbody>            
                </table>
            </div>        
        </div>
    </section>
</div>

<script src="<?php echo base_url() ?>application/views/alumno/js/listar.js"></script>
<script>
    $(".seleccionar").click(function(){
        var number = $(this).attr('data-number_id');
        
        var base_url = "<?php echo base_url()?>";
        window.location.href = base_url+"index.php/alumno/consultar/"+number;
    });
    
</script>
