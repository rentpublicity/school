<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-pencil-square-o"></i> Inscripción
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/inscripcion/crear">
                                <button type="button" class="btn btn-success">
                                    Nuevo
                                </button>
                            </a>                            
                        </div>
                        <div style="float: right; text-align: center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <table id="tabla" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Tipo documento</th>
                            <th>Documento</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Fecha</th>                                            
                        </tr>
                    </thead>
                    <tbody class="filas_tabla">
                        <tr class="seleccionar">
                            <td>
                                DNI
                            </td>
                            <td>
                                20725845
                            </td>
                            <td>
                                Alberto José
                            </td>
                            <td>
                                Atencio Palmar
                            </td>
                            <td>
                                01/01/2018
                            </td>
                        </tr>
                    </tbody>            
                </table>
            </div>        
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>application/views/inscripcion/js/listar.js"></script>