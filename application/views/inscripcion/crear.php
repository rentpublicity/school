<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-pencil-square-o"></i> Inscripción | Nuevo
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/inscripcion">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <form action="<?php echo current_url(); ?>" id="formInscripcion" method="post">
                        <div class="box box-info">
                            <div class="box-body">                                 
                                <div class="col-md-4">
                                    <label>Tipo de documento:</label> 
                                    <select class="form-control error">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Documento:</label> 
                                    <input id="documento" type="text" name="documento" class="form-control">          
                                </div>
                                <div class="col-md-4">
                                    <label>Cursos:</label> 
                                    <select class="form-control error select2">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                            </div>
                            <div style="text-align: center; margin: 20px">
                                <a>
                                    <button type="submit" class="btn btn-success">
                                        Aceptar
                                    </button>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/inscripcion">
                                    <button type="button" class="btn btn-default">
                                        Cancelar
                                    </button>
                                </a>
                            </div>
                            <br>
                        </div>  
                    </form> 
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>assets/js/validate.js"></script>