<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gear"></i> Usuarios | Nuevo
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/usuario">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <form action="<?php echo current_url(); ?>" id="form" method="post">
                        <div class="box box-info">
                            <div class="box-body"> 
                                <?php if (!empty($custom_error)) { ?>
                                    <div class="alert alert-danger"> <?php echo $custom_error; ?> </div>
                                    <?php }
                                ?>
                                <div class="col-md-4">
                                    <label>Usuario:</label> 
                                    <input id="usuario" type="text" name="usuario" class="form-control">
                                    <br>
                                    <label>Contraseña:</label> 
                                    <input id="clave" type="password" name="clave" class="form-control">
                                    <br>
                                    <label>Repetir Contraseña:</label> 
                                    <input id="clave2" type="password" name="clave2" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Tipo de documento:</label> 
                                    <select id="tipo_documento" name="tipo_documento" class="form-control error">
                                        <option value="">Seleccione</option>
                                    </select>
                                    <br>
                                    <label>Documento:</label> 
                                    <input id="documento" type="text" name="documento" class="form-control">
                                    <br>                                    
                                    <label>Primer nombre:</label> 
                                    <input id="primer_nombre" type="text" name="primer_nombre" value="" class="form-control">
                                    <br>
                                    <label>Segundo nombre:</label> 
                                    <input id="segundo_nombre" type="text" name="segundo_nombre" value="" class="form-control">
                                    <br>
                                    <label>Primer apellido:</label> 
                                    <input id="primer_apellido" type="text" name="primer_apellido" value="" class="form-control">
                                    <br>
                                    <label>Segundo apellido:</label> 
                                    <input id="segundo_apellido" type="text" name="segundo_apellido" value="" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Sexo:</label>
                                    <select id="sexo" name="sexo" class="form-control">
                                        <option value="">Seleccione</option>
                                        <option value="m">Masculino</option>
                                        <option value="f">Femenino</option>
                                    </select>
                                    <br>
                                    <label>Fecha de nacimiento:</label> 
                                    <input id="fecha_nacimiento" type="date" name="fecha_nacimiento" value="" class="form-control">
                                    <br>
                                    <label>Correo:</label> 
                                    <input id="email" type="email" name="email" value="" class="form-control">
                                    <br>
                                    <label>Teléfono:</label> 
                                    <input id="telefono" type="text" name="telefono" value="" class="form-control">
                                    <br>
                                    <label>Dirección:</label> 
                                    <textarea id="direccion" name="direccion" class="form-control" style="height: 113px"></textarea>
                                </div>
                            </div>
                            <div style="text-align: center; margin: 20px">
                                <a>
                                    <button type="submit" class="btn btn-success">
                                        Aceptar
                                    </button>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/usuario">
                                    <button type="button" class="btn btn-default">
                                        Cancelar
                                    </button>
                                </a>
                            </div>
                            <br>
                        </div>  
                    </form> 
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url() ?>assets/js/validate.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').validate({
            rules: {
                usuario: {required: true},
                clave: {required: true},
                tipo_documento: {required: true},
                documento: {required: true},
                primer_nombre: {required: true},
                primer_apellido: {required: true},
                fecha_nacimiento: {required: true},
                telefono: {required: true},
                fecha_ingreso: {required: true},
                sexo: {required: true},
                direccion: {required: true},
            },
            messages: {
                usuario: {required: 'Campo requerido.'},
                clave: {required: 'Campo requerido.'},
                tipo_documento: {required: 'Campo requerido.'},
                documento: {required: 'Campo requerido.'},
                primer_nombre: {required: 'Campo requerido.'},
                primer_apellido: {required: 'Campo requerido.'},
                fecha_nacimiento: {required: 'Campo requerido.'},
                telefono: {required: 'Campo requerido.'},
                fecha_ingreso: {required: 'Campo requerido.'},
                sexo: {required: 'Campo requerido.'},
                direccion: {required: 'Campo requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>