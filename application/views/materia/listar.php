<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gear"></i> Materias
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/materia/crear">
                                <button type="button" class="btn btn-success">
                                    Nuevo
                                </button>
                            </a>                            
                        </div>
                        <div style="float: right; text-align: center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <table id="tabla" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Descripcion</th>
                        </tr>
                    </thead>
                    <tbody class="filas_tabla">
                        <?php foreach($materias as $materia) { 
                            if($materia->activo == 1) { ?>
                        <tr data-codigo="<?php echo $materia->codigo ?>"class="seleccionar">
                            <td>
                                <?php echo $materia->codigo; ?>
                            </td>
                            <td>
                                <?php echo $materia->nombre; ?>
                            </td>
                        </tr>
                        <?php }
                        }
                        ?>
                    </tbody>            
                </table>
            </div>        
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>application/views/materia/js/listar.js"></script>
<script>
    $(".seleccionar").click(function(){
        var codigo= $(this).attr('data-codigo');
        window.location.href = "../index.php/materia/consultar/"+codigo;
    });
</script>