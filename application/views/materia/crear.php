<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gear"></i> Materias | Nuevo
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/materia">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <form action="<?php echo current_url(); ?>" id="form" method="post">
                        <div class="box box-info">
                            <div class="box-body"> 
                                <?php
                                if($this->session->flashdata('success') != null){?>
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i>¡Éxito!</h4>
                                        <?php echo $this->session->flashdata('success');?>
                                    </div>
                                <?php }?>

                                <?php if ($this->session->flashdata('error') != null || !empty(@$custom_error)){?>
                                      <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-check"></i> ¡Error!</h4>
                                        <?php echo $this->session->flashdata('error') . $custom_error;?>
                                    </div>
                                <?php }?>
                                <div class="col-md-6">
                                    <label>Código:</label> 
                                    <input id="codigo" type="text" name="codigo" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Descripción:</label> 
                                    <input id="nombre" type="text" name="nombre" value="" class="form-control">
                                </div>
                            </div>
                            <div style="text-align: center; margin: 20px">
                                <a>
                                    <button type="submit" class="btn btn-success">
                                        Aceptar
                                    </button>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/materia">
                                    <button type="button" class="btn btn-default">
                                        Cancelar
                                    </button>
                                </a>
                            </div>
                            <br>
                        </div>  
                    </form> 
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>assets/js/validate.js"></script>
<script type="text/javascript">
      $(document).ready(function(){
           $('#form').validate({
            rules :{
                  codigo:{ required:true },
                  nombre:{ required:true }
                  
            },
            messages:{
                codigo:{ required:'Campo requerido.' },
                nombre:{ required:'Campo requerido.' }
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });
      });
</script>