<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-gear"></i> Materias | Consultar
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/materia">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                        <div style="float: right; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/materia/modificar/<?php echo $materia->codigo;?>">
                                <button type="button" class="btn btn-warning">
                                    Modificar
                                </button>
                            </a>
                            <a href="<?php echo base_url(); ?>index.php/materia/eliminar/<?php echo $materia->codigo;?>">
                                <button type="button" class="btn btn-danger">
                                    Eliminar
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-9">
                    <div class="box box-info">
                            <div class="box-body">    
                                <div class="col-md-12">
                                    <label>Código:</label><?php echo $materia->codigo;?>  <br>
                                    <label>Descripcion:</label> <?php echo $materia->nombre;?>  <br>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box box-info">
                        <h3 style="margin-left: 5px">Datos de registro</h3>
                            <div class="box-body">    
                                <label>Usuario:</label> <?php echo  $materia->usuario_registro?> <br>
                                <label>Registro:</label> <?php echo dateFormat('d/m/Y',$materia->fecha_registro)?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
</div>