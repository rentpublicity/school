<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-calendar"></i> Cursos | Consultar
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/curso">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                        <div style="float: right; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/curso/modificar">
                                <button type="button" class="btn btn-warning">
                                    Modificar
                                </button>
                            </a>
                            <a href="<?php echo base_url(); ?>index.php/curso/eliminar">
                                <button type="button" class="btn btn-danger">
                                    Eliminar
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-9">
                    <div class="box box-info">
                        <div class="box-body">    
                            <div class="col-md-12">
                                <label>Sección:</label> Seccion <br>
                                <label>Materia:</label> Materia <br>
                                <label>Profesor:</label> Profesor <br>
                                <label>Inicio:</label> Inicio <br>
                                <label>Fin:</label> Fin <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box box-info">
                        <h3 style="margin-left: 5px">Datos de registro</h3>
                        <div class="box-body">    
                            <label>Usuario:</label> User1 <br>
                            <label>Registro:</label> 01/01/2018
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">                
                <div class="col-md-12">                    
                    <div class="box box-info">
                        <h3 style="margin-left: 20px">Alumnos</h2>
                            <div class="box-body">                                
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Tipo documento</th>
                                            <th>Documento</th>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                        </tr>
                                    </thead>
                                    <tbody class="filas_tabla">
                                        <tr class="seleccionarCurso">
                                            <td>
                                                DNI
                                            </td>
                                            <td>
                                                123456789
                                            </td>
                                            <td>
                                                Alberto José
                                            </td>
                                            <td>
                                                Atencio Palmar
                                            </td>                                            
                                        </tr>
                                    </tbody>            
                                </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>