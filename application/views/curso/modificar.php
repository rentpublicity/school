<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-calendar"></i> Cursos | Modificar
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/curso/consultar">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-body">    
                            <div class="col-md-6">
                                <label>Sección:</label> 
                                <select id="seccion" name="seccion" class="form-control error">
                                    <option value="">Seleccione</option>
                                </select>
                                <br>
                                <label>Materia:</label> 
                                <select id="materia" name="materia" class="form-control error">
                                    <option value="">Seleccione</option>
                                </select>
                                <br>
                                <label>Profesor:</label> 
                                <select id="profesor" name="profesor" class="form-control error">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Inicio:</label> 
                                <input id="fecha_inicio" type="date" name="fecha_inicio" value="" class="form-control">
                                <br>
                                <label>Fin:</label> 
                                <input id="fecha_fin" type="date" name="fecha_fin" value="" class="form-control">
                            </div>
                        </div>
                        <div style="text-align: center; margin: 20px">
                            <a href="<?php echo base_url(); ?>index.php/curso/consultar">
                                <button type="button" class="btn btn-success">
                                    Aceptar
                                </button>
                            </a>
                            <a href="<?php echo base_url(); ?>index.php/curso/consultar">
                                <button type="button" class="btn btn-default">
                                    Cancelar
                                </button>
                            </a>
                        </div>
                        <br>
                    </div>                    
                </div>
            </div>
        </div>
    </section>
</div>