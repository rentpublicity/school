<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-calendar"></i> Cursos | Nuevo
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/curso">
                                <button type="button" class="btn btn-success">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-12">
                    <form action="<?php echo current_url(); ?>" id="form" method="post">
                        <div class="box box-info">
                            <div class="box-body"> 
                                <?php if (!empty($custom_error)) { ?>
                                    <div class="alert alert-danger"> <?php echo $custom_error; ?> </div>
                                    <?php }
                                ?>
                                <div class="col-md-6">
                                    <label>Sección:</label> 
                                    <select id="seccion" name="seccion" class="form-control error">
                                        <option value="">Seleccione</option>
                                    </select>
                                    <br>
                                    <label>Materia:</label> 
                                    <select id="materia" name="materia" class="form-control error">
                                        <option value="">Seleccione</option>
                                    </select>
                                    <br>
                                    <label>empleado:</label> 
                                    <select id="empleadoe" name="empleadoe" class="form-control error">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Inicio:</label> 
                                    <input id="fecha_inicio" type="date" name="fecha_inicio" value="" class="form-control">
                                    <br>
                                    <label>Fin:</label> 
                                    <input id="fecha_fin" type="date" name="fecha_fin" value="" class="form-control">
                                </div>
                            </div>
                            <div style="text-align: center; margin: 20px">
                                <a>
                                    <button type="submit" class="btn btn-success">
                                        Aceptar
                                    </button>
                                </a>
                                <a href="<?php echo base_url(); ?>index.php/curso">
                                    <button type="button" class="btn btn-default">
                                        Cancelar
                                    </button>
                                </a>
                            </div>
                            <br>
                        </div>  
                    </form> 
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url() ?>assets/js/validate.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').validate({
            rules: {
                seccion: {required: true},
                materia: {required: true},
                empleado: {required: true},
                fecha_inicio: {required: true},
                fecha_fin: {required: true}
            },
            messages: {
                seccion: {required: 'Campo requerido.'},
                materia: {required: 'Campo requerido.'},
                empleado: {required: 'Campo requerido.'},
                fecha_inicio: {required: 'Campo requerido.'},
                fecha_fin: {required: 'Campo requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>