<div class="content-wrapper fondo" style="overflow-y: scroll; max-height: 500px">
    <section class="content-header">
        <h1 id="titulo">
            <i class="fa fa-book"></i> Empleados
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-body">   
                        <div style="float: left; text-align: center">
                            <a href="<?php echo base_url(); ?>index.php/empleado/crear">
                                <button type="button" class="btn btn-success">
                                    Nuevo
                                </button>
                            </a>                            
                        </div>
                        <div style="float: right; text-align: center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <table id="tabla" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Cargo</th>
                            <th>Tipo documento</th>
                            <th>Documento</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Ingreso</th>                                            
                            <th>Retiro</th>
                        </tr>
                    </thead>
                    <tbody class="filas_tabla">
                        <tr class="seleccionar">
                            <td>
                                Profesor
                            </td>
                            <td>
                                DNI
                            </td>
                            <td>
                                1234
                            </td>
                            <td>
                                nombres
                            </td>
                            <td>
                                apellidos
                            </td>
                            <td>
                                ingreso
                            </td>
                            <td>
                                retiro
                            </td>
                        </tr>
                    </tbody>            
                </table>
            </div>        
        </div>
    </section>
</div>
<script src="<?php echo base_url()?>application/views/empleado/js/listar.js"></script>