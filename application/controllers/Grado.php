<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class grado extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ((!$this->session->userdata('id')) || (!$this->session->userdata('log_in'))) {
            redirect('dashboard/login');
        }
        $this->load->helper('owndate');
        $this->load->model('grado_model', '', TRUE);
    }

    public function index() {
        $this->data['grados'] = $this->grado_model->getGrados();
        $this->data['view'] = 'grado/listar';
        $this->load->view('base/base', $this->data);
    }

    public function consultar() {
        $this->data['grado'] = $this->grado_model->getById($this->uri->segment(3));
        $this->data['view'] = 'grado/consultar';
        $this->load->view('base/base', $this->data);
    }

    public function crear() {
         $this->load->library('form_validation');
        if ($this->form_validation->run('grado') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {            
            $data_grado = array(
                'nombre' => set_value('nombre'),
                'usuario_registro' => $this->session->userdata('id')
            );
            if ($this->grado_model->add('grado', $data_grado) == TRUE) {
                redirect(base_url() . "index.php/grado/crear/");
            } else {
                $this->session->set_flashdata('error', 'Ha ocurrido un error');
            }
        }
        $this->data['grado'] = $this->grado_model->getById($this->uri->segment(3));
        $this->data['view'] = 'grado/crear';
        $this->load->view('base/base', $this->data);
    }
    
    public function unico_grado(){
        $id_grado = $this->uri->segment(3);
        if($id_grado!=NULL){
            $campo = "id!=";
        }
        else {
            $campo = "id=";
        }
        $nombre = set_value('nombre');
        $check = $this->grado_model->db->get_where('grado', array('nombre' => $nombre, $campo => $id_grado), TRUE);
        return !($check->num_rows() > 0);
    }

    public function modificar() {
         $this->load->library('form_validation');
        if ($this->form_validation->run('grado') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {            
            $id_grado = $this->uri->segment(3);
            $data_grado = array(
                'nombre' => set_value('nombre')
            );
            if ($this->grado_model->edit('grado', $data_grado, 'id', $id_grado) == TRUE) {
                redirect(base_url() . "index.php/grado/consultar/" . $this->uri->segment(3));
            } else {
                $this->session->set_flashdata('error', 'Ha ocurrido un error');
            }
        }
        $this->data['grado'] = $this->grado_model->getById($this->uri->segment(3));
        $this->data['view'] = 'grado/modificar';
        $this->load->view('base/base', $this->data);
    }

    public function eliminar() {
        $this->data['view'] = 'grado/eliminar';
        $this->load->view('base/base', $this->data);
    }

}
