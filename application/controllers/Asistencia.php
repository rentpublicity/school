<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class asistencia extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if((!$this->session->userdata('id')) || (!$this->session->userdata('log_in'))){
            redirect('dashboard/login');
        }
        //$this->load->model('mapos_model','',TRUE);
        
    }

    public function index() {
        $this->data['view'] = 'asistencia/listar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function consultar() {        
        $this->data['view'] = 'asistencia/consultar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function crear() {        
        $this->data['view'] = 'asistencia/crear';
        $this->load->view('base/base',  $this->data);
      
    }

    public function modificar() {        
        $this->data['view'] = 'asistencia/modificar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function eliminar() {        
        $this->data['view'] = 'asistencia/eliminar';
        $this->load->view('base/base',  $this->data);
      
    }
    
}
