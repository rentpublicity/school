<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class materia extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if((!$this->session->userdata('id')) || (!$this->session->userdata('log_in'))){
            redirect('dashboard/login');
        }
        $this->load->model('materia_model','',TRUE);
        $this->load->helper('owndate');
        
    }

    public function index() {
        $this->data['materias'] = $this->materia_model->getMaterias();
        $this->data['view'] = 'materia/listar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function consultar() {
        $this->data['materia'] = $this->materia_model->getById($this->uri->segment(3));   
        $this->data['view'] = 'materia/consultar';
        $this->load->view('base/base',  $this->data);
      
    }
    
    public function unico_nombre(){
        $id = set_value('id');
        $nombre = set_value('nombre');
        return !($this->materia_model->getBynombre($id,$nombre));
    }
    
    public function unico_codigo(){
        $id = set_value('id');
        $codigo = set_value("codigo");
        return !($this->materia_model->getByCodigo($id,$codigo));
    }

    public function crear() {
        /*if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aCliente')){
           $this->session->set_flashdata('error','No tiene permiso para agregar clientes.');
           redirect(base_url());
        }*/
        
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        
        if ($this->form_validation->run('materia') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
            
        } else {
            $data_materia = array(
                'codigo' => set_value('codigo'),
                'nombre' => set_value('nombre'),
                'usuario_registro' => $this->session->userdata('id')
            );
            if ($this->materia_model->add('materia', $data_materia) == TRUE) {
                $this->session->set_flashdata('success','¡Materia registrada satisfactoriamente!');
                    redirect(base_url() . 'index.php/materia/crear/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ha ocurrido un error.</p></div>';
            }
            
        }
        
        $this->data['view'] = 'materia/crear';
        $this->load->view('base/base',  $this->data);
      
    }
    
    

    public function modificar() { 
        /*if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aCliente')){
           $this->session->set_flashdata('error','No tiene permiso para agregar clientes.');
           redirect(base_url());
        }*/
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        
        if ($this->form_validation->run('materia') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
            
        } else {
            $id_materia = set_value('id');
            $data_materia = array(
                'codigo' => set_value('codigo'),
                'nombre' => set_value('nombre'),
                'usuario_registro' => $this->session->userdata('id')
            );
            if ($this->materia_model->edit('materia', $data_materia,'id',$id_materia) == TRUE) {
                $this->session->set_flashdata('success','¡Materia modificada satisfactoriamente!');
                    redirect(base_url() . 'index.php/materia/consultar/'.set_value('codigo'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>Ha ocurrido un error.</p></div>';
            }
            
        }
        $this->data['materia'] = $this->materia_model->getById($this->uri->segment(3));
        $this->data['view'] = 'materia/modificar';
        $this->load->view('base/base',  $this->data);
      
    }

    
    public function eliminar() {
        $codigo = $this->uri->segment(3);
        $resp = $this->input->get('r');
        if($resp!= NULL){
            $data_materia = array(
                'activo' => 0
            );
            if ($this->materia_model->edit('materia', $data_materia,'codigo',$codigo) == TRUE) {
                redirect(base_url() . 'index.php/materia');
            }
            
        }
        else{
             $this->data['view'] = 'materia/eliminar';
            $this->load->view('base/base',  $this->data);
        }
      
    }
    
}
