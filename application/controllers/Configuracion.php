<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class configuracion extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if((!$this->session->userdata('id')) || (!$this->session->userdata('log_in'))){
            redirect('dashboard/login');
        }
        //$this->load->model('mapos_model','',TRUE);
        
    }

    public function index() {
        $this->data['view'] = 'configuracion/listar';
        $this->load->view('base/base',  $this->data);
      
    }
    
}
