<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class parametros extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if((!$this->session->userdata('id')) || (!$this->session->userdata('log_in'))){
            redirect('dashboard/login');
        }
        $this->load->model('parametros_model','',TRUE);
        
    }

    public function index() {
        $this->data['parametros'] = $this->parametros_model->getAll();
        $this->data['view'] = 'parametros/listar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function consultar() {        
        $this->data['view'] = 'parametros/consultar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function crear() {        
        $this->data['view'] = 'parametros/crear';
        $this->load->view('base/base',  $this->data);
      
    }

    public function modificar() {
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        
        if ($this->form_validation->run('parametros') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
            
        } else {
            $id_parametro = $this->uri->segment(3);
            $data = array(
                'valor' => set_value('valor')
            );
            if ($this->parametros_model->edit('parametro', $data,'idparametro',$id_parametro) == TRUE) {
                $this->session->set_flashdata('success','Parámetro actualizado satisfactoriamente');
                
            } else {
                $this->session->set_flashdata('error','Ha ocurrido un error');
            }
        }
        $this->data['parametro'] = $this->parametros_model->getById($this->uri->segment(3));
        $this->data['view'] = 'parametros/modificar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function eliminar() {        
        $this->data['view'] = 'parametros/eliminar';
        $this->load->view('base/base',  $this->data);
      
    }
    
}
