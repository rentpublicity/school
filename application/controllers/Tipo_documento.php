<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tipo_documento extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if((!$this->session->userdata('id')) || (!$this->session->userdata('log_in'))){
            redirect('dashboard/login');
        }
        $this->load->model('Tipo_documento_model','',TRUE);
        
    }

    public function index() {
        $this->data['tipo_documentos'] = $this->Tipo_documento_model->getAll();
        $this->data['view'] = 'tipo_documento/listar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function consultar() {
        $this->data['tipo_documento'] = $this->Tipo_documento_model->getById($this->uri->segment(3));
        $this->data['view'] = 'tipo_documento/consultar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function crear() {        
        $this->data['view'] = 'tipo_documento/crear';
        $this->load->view('base/base',  $this->data);
      
    }

    public function modificar() {        
        $this->data['view'] = 'tipo_documento/modificar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function eliminar() {        
        $this->data['view'] = 'tipo_documento/eliminar';
        $this->load->view('base/base',  $this->data);
      
    }
    
}
