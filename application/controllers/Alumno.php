<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class alumno extends CI_Controller {
    private $parametros;
    public function __construct() {
        parent::__construct();
        if((!$this->session->userdata('id')) || (!$this->session->userdata('log_in'))){
            redirect('dashboard/login');
        }
        $this->load->helper('owndate');
        $this->parametros = $this->session->userdata('parametros');
        $this->load->model('persona_model','',TRUE);
        $this->load->model('alumno_model','',TRUE);
        
    }
    private function status(){
        $this->data['status_alumnos'] = $this->alumno_model->getStatus();
    }
    public function index() {
        $this->status();
        $this->data['alumnos'] = $this->alumno_model->getAlumnos();
        $this->data['view'] = 'alumno/listar';
        $this->load->view('base/base',  $this->data);
      
    }

    public function consultar() {
        $this->data['alumno'] = $this->alumno_model->getById($this->uri->segment(3));
        if(!is_object($this->data['alumno'])){
            $this->index();
            return;
        }
        $this->data['id'] = $this->uri->segment(3);
        $this->data['view'] = 'alumno/consultar';
        $this->load->view('base/base',  $this->data);
      
    }
    public function unico_documento(){
        $id_alumno = $this->uri->segment(3);
        $tipo_documento_id = set_value('tipo_documento');
        $documento = set_value('documento');
        if(($id_alumno)!= NULL){ // Caso editando
            if(is_numeric($id_alumno)){
                $id_persona = set_value('id_persona');
                $check = $this->persona_model->db->get_where('persona', array('tipo_documento_id' => $tipo_documento_id, 'documento' => $documento, 'id!=' => $id_persona), TRUE);
            }
        }
	else {// Caso crear
            $id_persona = !empty(set_value('id_persona')) ? set_value('id_persona') : NULL;
            $datos = array('tipo_documento_id' => $tipo_documento_id, 'documento' => $documento);
            if($id_persona != NULL)
                $datos['id!='] = $id_persona;
            $check = $this->persona_model->db->get_where('persona',$datos , 1);
        }
        return !($check->num_rows() > 0);
    }
    
    
    public function unico_email(){
        $id_alumno = $this->uri->segment(3);
        $email = set_value('email');
        if(($id_alumno)!= NULL){ // Caso editando
            if(is_numeric($id_alumno)){
                $id_persona = set_value('id_persona');
                $check = $this->persona_model->db->get_where('persona', array('email' => $email, 'id!=' => $id_persona), 1);
            }
        }
        else {// Caso crear
            $id_persona = !empty(set_value('id_persona')) ? set_value('id_persona') : NULL;
            $datos = array('email' => $email);
            if($id_persona != NULL)
                $datos['id!='] = $id_persona;
            $check = $this->persona_model->db->get_where('persona',$datos, 1);
        }
            
            
        return !($check->num_rows() > 0);
    }
    
    public function edad_inscripcion(){
        $edad_min_inscripcion = $this->parametros['edad_min_inscripcion'];
        $edad_max_inscripcion = $this->parametros['edad_max_inscripcion'];
        //die(($this->parametros['edad_inscripcion']));
        $fecha_nacimiento = set_value('fecha_nacimiento');
        $date1 = new DateTime($fecha_nacimiento);
        $date2 = $date1->diff(new DateTime(date('Y-m-d')));
        return !($date2->y < $edad_min_inscripcion || $date2->y > $edad_max_inscripcion );
    }
	
	
    public function crear() {
        /*if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aCliente')){
           $this->session->set_flashdata('error','No tiene permiso para agregar clientes.');
           redirect(base_url());
        }*/
        
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        
        if ($this->form_validation->run('alumno') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
            
        } else {
            $id_persona = !empty(set_value('id_persona')) ? set_value('id_persona') : NULL;
            $data_persona = array(
                'tipo_documento_id' => set_value('tipo_documento'),
                'documento' => set_value('documento'),
                'primer_nombre' => set_value('primer_nombre'),
                'segundo_nombre' => !empty(set_value('segundo_nombre')) ? set_value('segundo_nombre') : NULL,
                'primer_apellido' => set_value('primer_apellido'),
                'segundo_apellido' => !empty(set_value('segundo_apellido')) ? set_value('segundo_apellido') : NULL,
                'fecha_nacimiento' => set_value('fecha_nacimiento'),
                'email' => set_value('email'),
                'sexo' => set_value('sexo'),
                'telefono' => set_value('telefono'),
                'direccion' => set_value('direccion')
                
            );
            $data_alumno = array(
                'fecha_ingreso' => set_value('fecha_ingreso'),
                'fecha_retiro' => empty(set_value('fecha_retiro')) ? NULL : set_value('fecha_retiro'),
                'representante_principal' => set_value('representante_p'),
                'representante_secundario' => empty(set_value('representante_s')) ? NULL : set_value('representante_s'),
                'usuario_registro' => $this->session->userdata('id')
            );
            if($id_persona != NULL){
                if ($this->persona_model->edit('persona', $data_persona,'id',$id_persona) == TRUE) {
                    $data_alumno ['persona_id'] = $id_persona;
                    if ($this->alumno_model->add('alumno', $data_alumno) == TRUE) {
                        $this->session->set_flashdata('success','¡Alumno registrado satisfactoriamente!');
                        redirect(base_url() . 'index.php/alumno/crear/');
                    }
                } else {
                        $this->data['custom_error'] = '<div class="form_error"><p>Ha ocurrido un error.</p></div>';
                }
            }
            else{    
                if ($this->persona_model->add('persona', $data_persona) == TRUE) {
                    $data_alumno ['persona_id'] = $this->persona_model->db->insert_id();
                    if ($this->alumno_model->add('alumno', $data_alumno) == TRUE) {
                        $this->session->set_flashdata('success','¡Alumno registrado satisfactoriamente!');
                        redirect(base_url() . 'index.php/alumno/crear/');
                    }
                } else {
                    $this->data['custom_error'] = '<div class="form_error"><p>Ha ocurrido un error.</p></div>';
                }
            }
        }
        
        $this->status();
        $this->data['tipo_documentos'] = $this->persona_model->getTipoDocumentos();
        $this->data['view'] = 'alumno/crear';
        $this->load->view('base/base',  $this->data);
      
    }
    
    

    public function modificar() { 
		/*if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aCliente')){
           $this->session->set_flashdata('error','No tiene permiso para agregar clientes.');
           redirect(base_url());
        }*/
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        
        if ($this->form_validation->run('alumno') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
            
        } else {
            $id_persona = set_value('id_persona');
            $id_alumno = $this->uri->segment(3);
            $data_persona = array(
                'tipo_documento_id' => set_value('tipo_documento'),
                'documento' => set_value('documento'),
                'primer_nombre' => set_value('primer_nombre'),
                'segundo_nombre' => !empty(set_value('segundo_nombre')) ? set_value('segundo_nombre') : NULL,
                'primer_apellido' => set_value('primer_apellido'),
                'segundo_apellido' => !empty(set_value('segundo_apellido')) ? set_value('segundo_apellido') : NULL,
                'fecha_nacimiento' => set_value('fecha_nacimiento'),
                'email' => set_value('email'),
                'sexo' => set_value('sexo'),
                'telefono' => set_value('telefono'),
                'direccion' => set_value('direccion')
            );
            $data_alumno = array(
                'fecha_ingreso' => set_value('fecha_ingreso'),
                'fecha_retiro' => empty(set_value('fecha_retiro')) ? NULL : set_value('fecha_retiro'),
                'representante_principal' => set_value('representante_p'),
                'representante_secundario' => empty(set_value('representante_s')) ? NULL : set_value('representante_s'),
                'status' => set_value('status')
            );
            if ($this->persona_model->edit('persona', $data_persona,'id',$id_persona) == TRUE) {
                if ($this->alumno_model->edit('alumno', $data_alumno,'id',$id_alumno) == TRUE) {
                    redirect(base_url()."index.php/alumno/consultar/".$this->uri->segment(3));
                }
                else {
                    $this->session->set_flashdata('error','Ha ocurrido un error');
                }
            } else {
                $this->session->set_flashdata('error','Ha ocurrido un error');
            }
        }
        $this->status();
        $this->data['tipo_documentos'] = $this->persona_model->getTipoDocumentos();
        $this->data['alumno'] = $this->alumno_model->getById($this->uri->segment(3));
        $this->data['id'] = $this->uri->segment(3);
        $this->data['view'] = 'alumno/modificar';
        $this->load->view('base/base',  $this->data);
      
    }
    public function buscar_documento(){
        $tipo = $this->input->post('td');
        $documento = $this->input->post('d');
        echo json_encode($this->alumno_model->getByDocumento($tipo,$documento));
    }
    public function eliminar() {
        $this->data['view'] = 'alumno/eliminar';
        $this->load->view('base/base',  $this->data);
    }
    public function inactivar(){
        $id = $this->uri->segment(3);
        if ($this->alumno_model->edit('alumno', $data_alumno,'id',$id) == TRUE) {
            redirect(base_url()."index.php/alumno/listar");
        }
        else {
            $this->session->set_flashdata('error','Ha ocurrido un error');
        }
        
    }

    /*
    public function minhaConta() {
        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('mapos/login');
        }

        $this->data['usuario'] = $this->mapos_model->getById($this->session->userdata('id'));
        $this->data['view'] = 'mapos/minhaConta';
        $this->load->view('tema/topo',  $this->data);
     
    }

    public function alterarSenha() {
        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('mapos/login');
        }

        $oldSenha = $this->input->post('oldSenha');
        $senha = $this->input->post('novaSenha');
        $result = $this->mapos_model->alterarSenha($senha,$oldSenha,$this->session->userdata('id'));
        if($result){
            $this->session->set_flashdata('success','Contraseña modificada con éxito!');
            redirect(base_url() . 'index.php/mapos/minhaConta');
        }
        else{
            $this->session->set_flashdata('error','Ocurrió un error al modificar la contraseña!');
            redirect(base_url() . 'index.php/mapos/minhaConta');
            
        }
    }

    public function pesquisar() {
        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('mapos/login');
        }
        
        $termo = $this->input->get('termo');

        $data['results'] = $this->mapos_model->pesquisar($termo);
        $this->data['produtos'] = $data['results']['produtos'];
        $this->data['servicos'] = $data['results']['servicos'];
        $this->data['os'] = $data['results']['os'];
        $this->data['clientes'] = $data['results']['clientes'];
        $this->data['view'] = 'mapos/pesquisa';
        $this->load->view('tema/topo',  $this->data);
      
    }

    public function login(){
        
        $this->load->view('mapos/login');
        
    }
    public function sair(){
        $this->session->sess_destroy();
        redirect('mapos/login');
    }


    public function verificarLogin(){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email','Email','valid_email|required|xss_clean|trim');
        $this->form_validation->set_rules('senha','Senha','required|xss_clean|trim');
        $ajax = $this->input->get('ajax');
        if ($this->form_validation->run() == false) {
            
            if($ajax == true){
                $json = array('result' => false);
                echo json_encode($json);
            }
            else{
                $this->session->set_flashdata('error','Los datos de acceso son incorrectos.');
                redirect($this->login);
            }
        } 
        else {

            $email = $this->input->post('email');
            $senha = $this->input->post('senha');

            $this->load->library('encrypt');   
            $senha = $this->encrypt->sha1($senha);

            $this->db->where('email',$email);
            $this->db->where('senha',$senha);
            $this->db->where('situacao',1);
            $this->db->limit(1);
            $usuario = $this->db->get('usuarios')->row();
            
            if(is_object($usuario) > 0){
                $dados = array('nome' => $usuario->nome, 'id' => $usuario->idUsuarios,'permissao' => $usuario->permissoes_id , 'logado' => TRUE);
                $this->session->set_userdata($dados);

                if($ajax == true){
                    $json = array('result' => true);
                    echo json_encode($json);
                }
                else{
                    redirect(base_url().'mapos');
                }

                
            }
            else{
                
                
                if($ajax == true){
                    $json = array('result' => false);
                    echo json_encode($json);
                }
                else{
                    $this->session->set_flashdata('error','Los datos de acceso son incorrectos.');
                    redirect($this->login);
                }
            }
            
        }
        
    }


    public function backup(){

        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('mapos/login');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cBackup')){
           $this->session->set_flashdata('error','No tiene permiso para efectuar back-up.');
           redirect(base_url());
        }

        
        
        $this->load->dbutil();
        $prefs = array(
                'format'      => 'zip',
                'filename'    => 'backup'.date('d-m-Y').'.sql'
              );

        $backup =& $this->dbutil->backup($prefs);

        $this->load->helper('file');
        write_file(base_url().'backup/backup.zip', $backup);

        $this->load->helper('download');
        force_download('backup'.date('d-m-Y H:m:s').'.zip', $backup);
    }


    public function emitente(){   

        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('mapos/login');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
           $this->session->set_flashdata('error','No tiene permiso para configurar una empresa.');
           redirect(base_url());
        }

        $this->data['menuConfiguraciones'] = 'Configuraciones';
        $data['dados'] = $this->mapos_model->getEmitente();
        $data['view'] = 'mapos/emitente';
        $this->load->view('tema/topo',$data);
    }

    function do_upload(){
        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('mapos/login');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
           $this->session->set_flashdata('error','No tiene permiso para configurar una empresa.');
           redirect(base_url());
        }

        $this->load->library('upload');

        $image_upload_folder = FCPATH . 'assets/uploads';

        if (!file_exists($image_upload_folder)) {
            mkdir($image_upload_folder, DIR_WRITE_MODE, true);
        }

        $this->upload_config = array(
            'upload_path'   => $image_upload_folder,
            'allowed_types' => 'png|jpg|jpeg|bmp|JPEG||PNG||JPG||pdf||BMP||PDF',
            'max_size'      => 2048,
            'remove_space'  => TRUE,
            'encrypt_name'  => TRUE,
        );

        $this->upload->initialize($this->upload_config);

        if (!$this->upload->do_upload()) {
            $upload_error = $this->upload->display_errors();
            print_r($upload_error);
            exit();
        } else {
            $file_info = array($this->upload->data());
            return $file_info[0]['file_name'];
        }

    }


    public function cadastrarEmitente() {
        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('index.php/mapos/login');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
           $this->session->set_flashdata('error','No tiene permiso para configurar una empresa.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nome','Razão Social','required|xss_clean|trim');
        $this->form_validation->set_rules('cnpj','CNPJ','required|xss_clean|trim');
        $this->form_validation->set_rules('ie','IE','required|xss_clean|trim');
        $this->form_validation->set_rules('logradouro','Logradouro','required|xss_clean|trim');
        $this->form_validation->set_rules('numero','Número','required|xss_clean|trim');
        $this->form_validation->set_rules('bairro','Bairro','required|xss_clean|trim');
        $this->form_validation->set_rules('cidade','Cidade','required|xss_clean|trim');
        $this->form_validation->set_rules('uf','UF','required|xss_clean|trim');
        $this->form_validation->set_rules('telefone','Telefone','required|xss_clean|trim');
        $this->form_validation->set_rules('email','E-mail','required|xss_clean|trim');


        

        if ($this->form_validation->run() == false) {
            
            $this->session->set_flashdata('error','Los campos obligatorios están vacíos.');
            redirect(base_url().'index.php/mapos/emitente');
            
        } 
        else {

            $nome = $this->input->post('nome');
            $cnpj = $this->input->post('cnpj');
            $ie = $this->input->post('ie');
            $logradouro = $this->input->post('logradouro');
            $numero = $this->input->post('numero');
            $bairro = $this->input->post('bairro');
            $cidade = $this->input->post('cidade');
            $uf = $this->input->post('uf');
            $telefone = $this->input->post('telefone');
            $email = $this->input->post('email');
            $image = $this->do_upload();
            $logo = base_url().'assets/uploads/'.$image;


            $retorno = $this->mapos_model->addEmitente($nome, $cnpj, $ie, $logradouro, $numero, $bairro, $cidade, $uf,$telefone,$email, $logo);
            if($retorno){

                $this->session->set_flashdata('success','La información se ha introducido con éxito.');
                redirect(base_url().'index.php/mapos/emitente');
            }
            else{
                $this->session->set_flashdata('error','Se produjo un error al tratar de introducir la información.');
                redirect(base_url().'index.php/mapos/emitente');
            }
            
        }
    }


    public function editarEmitente() {
        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('index.php/mapos/login');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
           $this->session->set_flashdata('error','Usted no está autorizado a configurar una empresa.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nome','Razão Social','required|xss_clean|trim');
        $this->form_validation->set_rules('cnpj','CNPJ','required|xss_clean|trim');
        $this->form_validation->set_rules('ie','IE','required|xss_clean|trim');
        $this->form_validation->set_rules('logradouro','Logradouro','required|xss_clean|trim');
        $this->form_validation->set_rules('numero','Número','required|xss_clean|trim');
        $this->form_validation->set_rules('bairro','Bairro','required|xss_clean|trim');
        $this->form_validation->set_rules('cidade','Cidade','required|xss_clean|trim');
        $this->form_validation->set_rules('uf','UF','required|xss_clean|trim');
        $this->form_validation->set_rules('telefone','Telefone','required|xss_clean|trim');
        $this->form_validation->set_rules('email','E-mail','required|xss_clean|trim');


        

        if ($this->form_validation->run() == false) {
            
            $this->session->set_flashdata('error','Los campos obligatorios están vacíos.');
            redirect(base_url().'index.php/mapos/emitente');
            
        } 
        else {

            $nome = $this->input->post('nome');
            $cnpj = $this->input->post('cnpj');
            $ie = $this->input->post('ie');
            $logradouro = $this->input->post('logradouro');
            $numero = $this->input->post('numero');
            $bairro = $this->input->post('bairro');
            $cidade = $this->input->post('cidade');
            $uf = $this->input->post('uf');
            $telefone = $this->input->post('telefone');
            $email = $this->input->post('email');
            $id = $this->input->post('id');


            $retorno = $this->mapos_model->editEmitente($id, $nome, $cnpj, $ie, $logradouro, $numero, $bairro, $cidade, $uf,$telefone,$email);
            if($retorno){

                $this->session->set_flashdata('success','La información se ha cambiado correctamente.');
                redirect(base_url().'index.php/mapos/emitente');
            }
            else{
                $this->session->set_flashdata('error','Se produjo un error al tratar de cambiar la información.');
                redirect(base_url().'index.php/mapos/emitente');
            }
            
        }
    }


    public function editarLogo(){
        if((!$this->session->userdata('session_id')) || (!$this->session->userdata('logado'))){
            redirect('index.php/mapos/login');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){
           $this->session->set_flashdata('error','Usted no está autorizado a configurar una empresa.');
           redirect(base_url());
        }

        $id = $this->input->post('id');
        if($id == null || !is_numeric($id)){
           $this->session->set_flashdata('error','Se produjo un error al intentar cambiar el logomarca.');
           redirect(base_url().'index.php/mapos/emitente'); 
        }
        $this->load->helper('file');
        delete_files(FCPATH .'assets/uploads/');

        $image = $this->do_upload();
        $logo = base_url().'assets/uploads/'.$image;
        $image_file = file_get_contents($logo);
        @file_put_contents("logo.jpeg", $image_file);
        $retorno = $this->mapos_model->editLogo($id, $logo);
        if($retorno){

            $this->session->set_flashdata('success','La información se ha cambiado correctamente.');
            redirect(base_url().'index.php/mapos/emitente');
        }
        else{
            $this->session->set_flashdata('error','Se produjo un error al tratar de cambiar la información.');
            redirect(base_url().'index.php/mapos/emitente');
        }

    }
*/
    
}
